#!/usr/bin/env bash

## Generate CHANGELOG.md based on the commit messages and tags.
## These are sorted by tags in descending order.
##
## The script is based on https://github.com/aldrin/git-changelog
##
## Instructions: 
##    1. Update `.changelog.yml` according to your preference
##    2. Run `bash generate_changelog.sh $1 $2`
##       a. Update the project directory where you want to generate the changelog. 
##          Note that if previous_tag has no value, the script will generate the logs from the oldest tag
##              
##              bash generate_changelog.sh <project_directory>/CHANGELOG.md <previous_tag>
##
##       b. In deployment_tool, use the script below:
##
##              bash generate_changelog.sh > ../CHANGELOG.md 

### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

## Get user input
file=$1
previous_tag=$2

## Add default
[ $file == ""] && file="CHANGELOG.md"
[ $previous_tag == ""] && previous_tag=0

## Check if cargo is available in the environment
if ! command -v cargo &> /dev/null
then
    {
        echo "$BLUE ---------------------------------------------------- $RESET"
        echo "$BLUE Installing the prerequisites.. $RESET"
        echo;

        set -x

        apt-get install cargo
        cargo install git-changelog
        export PATH="$HOME/.cargo/bin:$PATH"
        
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    } || {
        set +x
        echo;
        echo "$RED ☒  Error in installing the prerequisites. $RESET"
        echo;
        exit 1
    }
fi

## Check if git changelog is available in the environment
if ! command -v "git changelog" &> /dev/null
then
    {
        echo "$BLUE ---------------------------------------------------- $RESET"
        echo "$BLUE Installing the prerequisites.. $RESET"
        echo;

        set -x

        export PATH="$HOME/.cargo/bin:$PATH"
        
        set +x
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    } || {
        set +x
        echo;
        echo "$RED ☒  Error in installing the prerequisites. $RESET"
        echo;
        exit 1
    }
fi

## Generate the changelog 

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Generating the changelog... $RESET"
echo;
{
    set -x
    if [ -f "$file" ];then
        ## If file is existing, the changes will be appended
        for current_tag in $(git tag --sort=-creatordate)
        do
            if [ "$previous_tag" != 0 ];then
                git changelog ${current_tag}...${previous_tag} | grep -v Merge | cat - "$file" > temp && mv temp "$file"
                printf "\n\n"
            fi
            previous_tag=${current_tag}
        done

        set +x
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
        echo "$GREEN ---------------------------------------------------- $RESET"
        echo "$GREEN ☑  $file is updated. $RESET"
        echo;
    else
        for current_tag in $(git tag --sort=-creatordate)
        do
            if [ "$previous_tag" != 0 ];then
                git changelog ${current_tag}...${previous_tag} | grep -v Merge
                printf "\n\n"
            fi
            previous_tag=${current_tag}
        done > "$file"

        set +x
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
        echo "$GREEN ---------------------------------------------------- $RESET"
        echo "$GREEN ☑  $file is created. $RESET"
        echo;
    fi

    ## Remove lines with Approved-by:
    sed -i '/.*Approved-by:.*/d' "$file"
} || {
    set +x
    echo;
    echo "$RED ☒  Error in generating the changelog. $RESET"
    echo;
}