#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message

#-----------------------------------------------------------------------------#
### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

#-----------------------------------------------------------------------------#
# Statement for start

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Starting the deployment of ObiOne... $RESET"
echo;

#-----------------------------------------------------------------------------#

## Retrieve current directory
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [1/5] Retrieving current directory.. $RESET"
echo;

pwd=`pwd`
export OBIONE_DATA_DIR=$pwd

echo $OBIONE_DATA_DIR

## Create directories for MongoDB
set -x
mkdir -p do-volume/do-obione-db/data
set +x

echo;
echo "$GREEN ☑  DONE. $RESET"
echo;

#-----------------------------------------------------------------------------#

## Load environment variables containing parameters for ObiOne network and stack
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [2/5] Loading ObiOne System variables.. $RESET"
echo;

set -a
set -x 
. config/do_system_config
set +x
set +a

echo;
echo "$GREEN ☑  DONE. $RESET"
echo;

#-----------------------------------------------------------------------------#

## Create Docker network for ObiOne
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [3/5] Creating ObiOne network.. $RESET"
echo;

{
    docker network create --ingress --driver overlay $OBIONE_DOCKER_NETWORK_NAME && {
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    }
} || {
    echo;
    echo "$YELLOW ⚠  ObiOne network already exists. $RESET"
    echo;
}

#-----------------------------------------------------------------------------#

## Pull Docker images
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [4/5] Loading Docker images.. $RESET"
echo;

{
    bash utility/pull_images.sh env && {
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    }
} || {
    echo;
    echo "$RED ☒  Error in loading Docker images. $RESET"
    echo;
    exit 1
}

#-----------------------------------------------------------------------------#

## Load environment variables containing parameters for Core System stack and deploy

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [5/5] Deploying stack... $RESET"
echo;

{
    set -x
    env $(cat env | grep ^[A-Z] | xargs) \
        docker stack deploy --with-registry-auth -c do-obione-stack.yml $OBIONE_DOCKER_STACK_NAME && {
            set +x
            echo;
            echo "$GREEN ☑  DONE. $RESET"
            echo;
            echo "$GREEN ---------------------------------------------------- $RESET"
            echo "$GREEN ☑  Deployment is complete. $RESET"
            echo;
        }
} || {
    set +x
    echo;
    echo "$RED ☒  Error in deploying stack. Please run the script again. $RESET"
    echo;
}