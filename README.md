# ObiOne Deployment Tool #

> This repository contains all the necessary resources to deploy ObiOne stack. It heavily relies on Docker and Docker Swarm technologies in order for the services to interact together.
## Table Of Contents
* [General info](#markdown-header-general-info)
    * [Directory Structure](#markdown-header-directory-structure)
* [Getting Started](#markdown-header-getting-started)
    * [Prerequisites](#markdown-header-prerequisites)
* [Contact](#markdown-header-contact)

## General info

### Directory Structure

	|── config								# Contains configuration files for managing the ObiOne Docker Swarm
	|── do-volume						    # Contains the data for DB and Traefik
    |__ utility                  			# Utility scripts for supporting CI/CD processes as well as to support DevOps tasks in the ObiOne stack

## Getting Started

### Prerequisites

#### Docker Swarm Cluster
⚠️ 1. A pre-initialized docker swarm cluster

#### User Account and Access
1. Docker Hub account (with access to images)
2. BitBucket account (with access to to all repositories indicated in this guide)

#### Hardware Requirements:
1. 100 GB HDD
2. 4 GB RAM
3. 2 CPU Cores

#### Software Requirements

1. Ubuntu 18.04 LTS or CentOS 7 (these are recommended over Windows because of configuration and compatibility issues that tend to come up more often in Windows)
2. Git
3. Docker Engine CE (v19.03+)

### Checking out and deploying

[1] Clone the ObiOne Deployment Tool repository and change your current directory to `do-obione-deployment-tool`:

```bash
git clone https://bitbucket.org/ebsproject/do-obione-deployment-tool.git 

cd do-obione-deployment-tool
```

[2] Update the following files based on the environment to be used in deployment (i.e localhost)

_env_
```bash
OBIONE_API_HOST=xxxx
```

_config/do_system_config_
- Docker stack and network name
```bash
## Default values
OBIONE_DOCKER_STACK_NAME=do
OBIONE_DOCKER_NETWORK_NAME=obione_network
```

[3] Deploy the stack

```bash
bash deploy_stack.sh
```

#### ⚠️ **_To bring down the infrastructure, execute:_**

```bash
docker stack rm <OBIONE_DOCKER_STACK_NAME>
```